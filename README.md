# JVM labfolder API wrapper

by Stephan Janosch, MPI-CBG.de

minimal project goals:
- [x] create entry
- [ ] add data elements to entry
- [ ] add file elements to entry
- [ ] provide .jar for reuse in other JVM related projects

right now it's not possible to
- create text elements
- ...
## Usage

Always validate your token!
`de.mpicbg.sweng.labfolderapiwrapper.LabfolderServer.validateToken()`

## Examples

Please check `src/main/groovy/de/mpicbg/sweng/labfolderapiwrapper/examples`

## token reuse

Token is stored in `~/.labfolder_token.sh`. If file does not exist, it will be created. This token
cache is also used by [Labfolder Git pre push hook](https://gitlab.gwdg.de/labfolder-api-tools/git-hook)

## limitations

- No user registration, so user must have created account beforehand
- no logging
- as of Oct 2019 the labfolder API is does not provide the endpoints
   - access text elements
   - access data elements 

## Requirement

* https://github.com/http-builder-ng/http-builder-ng

## original REPO

git@gitlab.gwdg.de:labfolder-api-tools/jvm-labfolderapiwrapper.git
