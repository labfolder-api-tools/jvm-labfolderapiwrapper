package de.mpicbg.sweng.labfolderapiwrapper

@groovy.transform.Canonical
class UserCredentials {
    String user
    String password

    UserCredentials(String user, String password) {
        this.user = user
        this.password = password
    }
}

