package de.mpicbg.sweng.labfolderapiwrapper.examples

import de.mpicbg.sweng.labfolderapiwrapper.LabfolderServer

class ProjectCountExample {

    static void main(String[] args){
        def username = 'USERNAME'
        def password = 'PASSWORD'

        if (username.equals('USERNAME'))
        {
            println 'Please set username and password. Exiting'
            return
        }

        LabfolderServer server = new LabfolderServer(new URL("https://eln.labfolder.com/"),username,password)
        def projects = server.getAllVisibleProjects()
        println "${projects.size()} projects found in ${server}"

//        //find the ones in the correct groups
//        def groupsOfInterest = ['34','36']
//        def filteredProjects = projects.findAll { groupsOfInterest.contains(it.group_id)}
//
//        //output results
//        println "In groups $groupsOfInterest are ${filteredProjects.size()} projects.\n"
//        filteredProjects.each {println it.title}

    }
}
