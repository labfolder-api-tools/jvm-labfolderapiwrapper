package de.mpicbg.sweng.labfolderapiwrapper

import groovyx.net.http.FromServer
import groovyx.net.http.HttpBuilder
import groovyx.net.http.ContentTypes

import java.time.format.DateTimeFormatter


class LabfolderServer {
    private URL url
    private String urlPrefix = 'api/v2/'
    private String username
    private String password
    private String token

    private static String userAgent = 'JVM Labfolder Api wrapper;janosch@mpi-cbg.de'
    private static String tokenFilename = '.labfolder_token.sh'
    //2016-12-09T08:17:07.000+0100
    public final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

    LabfolderServer(URL url, String token) {
        this.url = url
        this.token = token
    }

    /**
     * a constructor
     * @param url e.g. https://eln.labfolder.com/
     * @param username
     * @param password
     */
    LabfolderServer(URL url, String username, String password) {
        this.url = url
        this.username = username
        this.password = password
        this.token = getUserToken()
    }

    private HttpBuilder getBuilder(String path){
        HttpBuilder builder = HttpBuilder.configure {
            request.uri = url.toString()
            request.uri.path = urlPrefix+path
            request.headers['User-Agent'] = userAgent
            request.headers['Authorization'] = ' Token '+this.token
            request.headers['Accept'] = '*/*'
//            request.headers['Accept-Encoding'] = 'gzip'
//            response.parser(ContentTypes.JSON[0]){ config, resp ->
//                NativeHandlers.Parsers.json(config, resp).json as Project[]
//            }
        }
        return builder
    }

    /**
     * It's adviced to use @see Project#createEntry(java.lang.String).
     * @param entry
     * @return A new entry instance containing all information
     */
    Entry createEntry(Entry entry){
        if (!validateToken()) {
            println "No valid token"
            return null
        }
        if (!entry.project_id)
            throw new Exception("Entry needs project_id")

        def result
        HttpBuilder myBuilder = getBuilder('entries')
        myBuilder.post{
            request.body =entry
            request.contentType =ContentTypes.JSON[0]
            response.failure { FromServer fromServer, Object body ->
                println 'Failure '+fromServer.statusCode
                println body
                fromServer.getHeaders().each {println it}
            }
            response.success {FromServer fs, Object body ->
                result = body
            }
        }
        if (!result)
            return null
        Entry resultEntry
        resultEntry = new Entry(result)
        return resultEntry
    }

    Project findProjectById(String projectId)
    {
        if (!validateToken()) {
            println "No valid token"
            return null
        }
        def result
        HttpBuilder myBuilder = getBuilder('projects')
        myBuilder.get{
            request.uri.query = [limit:10,project_ids:projectId]
            response.failure { FromServer fromServer, Object body ->
                println 'Failure '+fromServer.statusCode
                println body
                fromServer.getHeaders().each {println it}
            }
            response.success {FromServer fs, Object body ->
                result = body
            }
        }
        if (!result)
            return null
        Project project = new Project(result[0])
        return  project
    }

    List<Project> getAllVisibleProjects()
    {
        if (!validateToken()) {
            println "No valid token"
            return null
        }

        FromServer.Header.CombinedMap linkMap
        def result
        HttpBuilder myBuilder = getBuilder('projects')

        myBuilder.get{
            request.uri.query = [limit:1000]
            response.failure { FromServer fromServer, Object body ->
                println 'Failure '+fromServer.statusCode
                println body

                fromServer.getHeaders().each {println it}
            }
            response.success {FromServer fs, Object body ->
                linkMap = (FromServer.Header.CombinedMap)(fs.headers.find{ h->h.key == 'Link'})
                result = body
            }
        }

        def projects = []
        if (result)
            result.each{
                projects += new Project(it)
            }

        String nextLink
        nextLink = LabfolderUtilities.getNextPaginationLink(linkMap)
//        println nextLink
        while (nextLink)
        {
            linkMap = null
            myBuilder.get {
                request.raw = nextLink
                response.success {FromServer fs, Object body ->
                    linkMap = (FromServer.Header.CombinedMap)(fs.headers.find{ h->h.key == 'Link'})
                    result = body
                }

            }
            if (result)
                result.each{
                    Project p =  new Project(it)
                    p.labfolderServer = this
                    projects += p
                }
            nextLink = LabfolderUtilities.getNextPaginationLink(linkMap)
        }
        return projects
    }


    private static String getUserToken(){
        File labfolderTokenfile = new File(System.getProperty('user.home')+"/$tokenFilename")
        if (!labfolderTokenfile.exists())
            return null
        String tokenLine = labfolderTokenfile.find{it.contains('TOKEN')}
        String token=tokenLine.split('=')[1].replaceAll('"','')
        return token
    }

    private static saveUserToken(String token){
        if (token ==null)
            throw new NullPointerException('No token given')
        File labfolderTokenfile = new File(System.getProperty('user.home')+"/$tokenFilename")
        if (labfolderTokenfile)
            labfolderTokenfile.delete()
        labfolderTokenfile.createNewFile()
        labfolderTokenfile.withWriter('utf-8') {writer ->
            writer.writeLine('#!/usr/bin/env bash')
            writer.writeLine("TOKEN=\"$token\"")
        }

    }

    private boolean validateToken(){
        boolean firstTest = false
        def myRequest = HttpBuilder.configure {
            request.uri = url.toString()
            request.uri.path = urlPrefix+'projects'
            request.headers['User-Agent'] = userAgent
            request.headers['Authorization'] = ' Token '+this.token
            request.headers['Accept'] = '*/*'
        }
        myRequest.get{
            request.uri.query = [limit:1000]
            response.failure { FromServer fromServer, Object body ->
                firstTest = false
            }
            response.success {FromServer fs, Object body ->
                firstTest = true
            }
        }
        if (firstTest)
            return true
        acquireAndSaveNewToken()
        return true
    }

    private acquireAndSaveNewToken(){
        if (!this.username || !this.password)
            throw new Exception("Username and password needed for acquiring new token!")
        String token
        def myRequest = HttpBuilder.configure {
            request.uri = url.toString()
            request.uri.path = urlPrefix+'auth/login'
            request.contentType = ContentTypes.JSON[0]
            request.headers['User-Agent'] = userAgent
            request.headers['Authorization'] = ' Token '+this.token
            request.headers['Accept'] = '*/*'
        }
        def result
        myRequest.post{
//            request.body = ["user":this.username,"password":this.password]
//            request.body = "{\"user\":\"${this.username}\",\"password\":\"${this.password}\"}"
            request.body = new UserCredentials(this.username,this.password)
            response.failure { FromServer fs, Object body ->
                println fs.statusCode
//                fs.headers.each {println it}
                println body
                println request.body
            }
            response.success { FromServer fs, Object body ->
                result = body
            }
        }
        if (result) {
            token = result.token
        }
        if (token)
        {
            this.token = token
            saveUserToken(token)
        }
    }


    @Override
    String toString() {
        return "LabfolderServer{" +
                "url=" + url +
                ", urlPrefix='" + urlPrefix + '\'' +
                ", username='" + username + '\'' +
                '}'
    }
}
