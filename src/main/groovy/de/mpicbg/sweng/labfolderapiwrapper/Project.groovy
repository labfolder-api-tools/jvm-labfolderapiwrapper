package de.mpicbg.sweng.labfolderapiwrapper

import java.time.OffsetDateTime

@groovy.transform.Canonical
class Project {
    String title
    String id
    String owner_id
    String group_id
    String folder_id
    Boolean hidden
    OffsetDateTime creation_date
    OffsetDateTime version_date

    LabfolderServer labfolderServer

    void setCreation_date(String dateString) {
        this.creation_date = OffsetDateTime.parse(dateString, LabfolderServer.formatter )
    }

    void setVersion_date(String dateString) {
        this.version_date = OffsetDateTime.parse(dateString,LabfolderServer.formatter)
    }


    /**
     * create an entry within Project
     * @param title
     * @param project_id
     * @return
     */
    Entry createEntry(String title)
    {
        if (!labfolderServer)
        {
            throw new Exception("Project has no server information.")
        }
        if (!this.id)
        {
            throw new Exception("Projects without ID cannot create Entries")
        }

        Entry entryToCreate = new Entry(title: title,project_id:this.id)
        Entry resultEntry = this.labfolderServer.createEntry(entryToCreate)
        resultEntry.project = this
        return  resultEntry
    }


    @Override
    String toString() {
        return "Project{" +
                "title='" + title + '\'' +
                ", id='" + id + '\'' +
                ", creation_date=" + creation_date +
                '}'
    }
}
