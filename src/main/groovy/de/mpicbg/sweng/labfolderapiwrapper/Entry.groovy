package de.mpicbg.sweng.labfolderapiwrapper

import java.time.OffsetDateTime

@groovy.transform.Canonical
class Entry {

    //labfolder attributes
    String title
    String project_id
    String id
    String  version_id
    String author_id
    OffsetDateTime creation_date
    OffsetDateTime version_date
    Integer entry_number
    Boolean hidden
    Boolean editable
    ElementLayout element_layout

    def tags=[]
    def elements = []
    def custom_dates = []

    //attributes for API
    Project project

    void setCreation_date(String creation_date) {
        this.creation_date = OffsetDateTime.parse(creation_date, LabfolderServer.formatter )
    }

    void setVersion_date(String creation_date) {
        this.version_date = OffsetDateTime.parse(creation_date, LabfolderServer.formatter )
    }

    @Override
    String toString() {
        return "Entry{" +
                "title='" + title + '\'' +
                ", project_id='" + project_id + '\'' +
                ", id='" + id + '\'' +
                ", version_id='" + version_id + '\'' +
                ", author_id='" + author_id + '\'' +
                ", creation_date=" + creation_date +
                ", version_date=" + version_date +
                ", entry_number=" + entry_number +
                ", hidden=" + hidden +
                ", editable=" + editable +
                ", element_layout=" + element_layout +
                ", tags=" + tags +
                ", elements=" + elements +
                ", custom_dates=" + custom_dates +
                ", project=" + project +
                '}'
    }

    class ElementLayout{
        Object[] rows

    }


}
