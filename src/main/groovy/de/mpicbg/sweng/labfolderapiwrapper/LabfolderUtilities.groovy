package de.mpicbg.sweng.labfolderapiwrapper

import groovyx.net.http.FromServer

class LabfolderUtilities {

    /**
     * A method for extracting the next page link from a Link-Header
     * @param linkMap
     * @return the link to the next page given by the server
     */
    public static String getNextPaginationLink(FromServer.Header.CombinedMap linkMap)
    {
        if (linkMap){
        def links = linkMap.value.split(';')
            if (links.find{it.contains('rel="next"')})
                return links.find{it.contains('rel="next"')}.split(' ')[0]
        }
        return null
    }


}
